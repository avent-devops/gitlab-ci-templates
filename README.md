# GitLab CI Templates

## Overview

This repository contains a set of pre-defined GitLab CI templates.

## Templates

### Deploy to ECS (AWS)

Deploy your projects easily to an ECS cluster, with the help of a CloudFormation template that automatically creates all
the necessary resources on your behalf.

This CloudFormation template will take care of :

- Create a task definition
- Create an associated service
- Create a target group, and a listener rule to expose the service to internet over an ELB

Note that this template requires some setup. Please check the [pipeline guide](ecs/README.md) before making your
deployments using this template.
