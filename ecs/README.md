# Deploy to ECS (AWS)

## Introduction

This pipeline template was created as a way to drastically simplify deployments to an ECS cluster.  
Under the hood, it relies on a CloudFormation template that takes care of creating all the necessary resources for
deploying an application and expose it on the web.

### Summary

To sum it up, this pipeline will mainly :

- Build your Docker image and push it to your GitLab repository's Docker image registry
- Create an ECS service that runs on FARGATE
- Create an ELB target group attached to the previously created service
- Add a listener rule to your ELB, that will listen for a defined hostname
- Spawn your container on your ECS cluster and specified VPC, subnets and security groups
- Send your container's logs to CloudWatch
- If needed, upload your environment variables file to an S3 bucket and attach it to the container

### Drawbacks

As the provided CloudFormation template tends to be the more generic possible with the less configuration needs, it also
comes with some drawbacks :

- Your VPC should have public subnets
- Your app can only have one container
- Your container is expected to listen to the port `80`
- Your container will have `256` vCPU capacity and `512` Mb of RAM
- The created service is configured to spawn 2 replicas
- Autoscaling is not supported yet

For more details, see the [CloudFormation template](cloudformation-stack.yml).

## Prerequisites

Before using this template, you must make sure that you already :

- Created an IAM user account with programmatically access (1)
- If you're using a private registry, created a secret that contains repository authentication information
- Created an ECS cluster
- Created a VPC with public subnets
- Created security groups for your tasks
- Created an Elastic Load Balancer
- Created an ECS task execution role (2)

**Note 1:** Your IAM user should have access to the following AWS services :

- CloudFormation
- ECS
- ElasticLoadBalancing
- S3

**Note 2:** Your role should also have policies that enables it to :

- If you're using a private registry, pull images from it
- Read files from the S3 bucket where environment files are stored
- Create CloudWatch log groups

## Usage

### Required CI environment variables

The table below shows all the CI variables that you **must** define in your project CI configuration :

| Name                                  | Type          | Description                                             |
| --------------------------------------|---------------|---------------------------------------------------------|
| AWS_ACCESS_KEY_ID                     | VARIABLE      | AWS access key ID                                       |
| AWS_SECRET_ACCESS_KEY                 | VARIABLE      | AWS access key secret                                   |
| AWS_DEFAULT_REGION                    | VARIABLE      | AWS default region                                      |
| CF_CLUSTER_NAME                       | VARIABLE      | Name of the ECS cluster where app will be deployed on   |
| CF_HOSTNAME                           | VARIABLE      | Hostname that the created service should listen for     |
| CF_REPOSITORY_CREDENTIALS_ARN         | VARIABLE      | ARN of the secret containing repository credentials     |
| CF_TASK_EXECUTION_ROLE_ARN            | VARIABLE      | ECS task execution role ARN                             |
| CF_VPC_ID                             | VARIABLE      | Id of the VPC that the created service will connect to  |
| CF_VPC_SUBNETS                        | VARIABLE      | Comma-separated list of VPC subnets to attach           |
| CF_VPC_SECURITY_GROUPS                | VARIABLE      | Comma-separated list of security groups to attach       |
| CF_ELB_LISTENER_ARN                   | VARIABLE      | ELB listener ARN                                        |

### Optional CI environment variables

The table below shows all the CI variables that are optional, depending on your needs :

| Name                                  | Type          | Description                                             |
| --------------------------------------|---------------|---------------------------------------------------------|
| CF_CONTAINER_ENV_BUCKET               | VARIABLE      | S3 Bucket name where env files should be uploaded to    |
| CF_CONTAINER_ENV_FILE                 | FILE          | Container environment variables file                    |

### Example

Below is a sample `.gitlab-ci.yml` that makes use of this pipeline :

```yaml
include:
    remote: 'https://gitlab.com/avent-devops/gitlab-ci-templates/raw/master/ecs/ecs.gitlab-ci.yml'

package:
    extends: .build-image
    stage: build
    only:
        - master

production:
    extends: .deploy-stack
    stage: deploy
    only:
        - master
    environment:
        name: production
```

## Known issues

- As environment files are directly uploaded to an S3 bucket, outside the CloudFormation template, those files are not
  removed automatically when the CloudFormation stack is deleted.
